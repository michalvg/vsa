/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ent;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author vsa
 */
@Entity
@Table(name = "T_OSOBA")
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "meno")
    private String name;
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "narodena")
    private Date born;
    @Column(nullable = false, name = "vaha") 
    private Float weight;
    //@Column(name = "narodena")
    @Transient
    private int age;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAge() {
        
        Date cur = new Date();
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        if(born == null) return 0;
        int d1 = Integer.parseInt(formatter.format(born));
        int d2 = Integer.parseInt(formatter.format(cur));
        age = (d2 - d1)/ 10000;
        
        return age;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Person)) {
            return false;
        }
        Person other = (Person) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ent.Person[ id=" + id + ", meno=" + name + ", narodena=" + age + ", vaha=" + weight + ", vek=" + getAge() + " ]";
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }
    
    
    
}

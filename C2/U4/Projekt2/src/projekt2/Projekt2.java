/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;

import ent.Person;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author vsa
 */
public class Projekt2 {

    /**
     * @param args the command line arguments
     */
    private EntityManagerFactory emf;
    private EntityManager em;

    public Projekt2() {
        this.emf = Persistence.createEntityManagerFactory("Projekt2PU");
        this.em = emf.createEntityManager();
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        //EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projekt2PU");
        //EntityManager em = emf.createEntityManager();
        
        Projekt2 show = new Projekt2();
       // try{
            
            show.showAllPersons();
            System.out.println(show.addPerson("Joyko", 45));
            Long x = show.findPerson(new Long(29));
            if(x == -1) System.out.println("Osoba neexistuje");
            else System.out.println("osoba existuje s id: " + x);
            
        /*}
        catch(Exception e){
            System.out.println(e);
        }*/
    }
    
    public void showAllPersons(){
        Query q = em.createNativeQuery("select * from t_osoba", Person.class);
        List<Person> listOfPerson = q.getResultList();
        
        for(Person i: listOfPerson){
            System.out.println(i.toString());
        }
    }
    
    public Long addPerson(String meno, float vaha){
        
        Person o = new Person();
        o.setName(meno);
        o.setWeight(vaha);
        em.getTransaction().begin();
        em.persist(o);
        em.getTransaction().commit();
        
        return o.getId();
    }
    
    public Long findPerson(Long id){
        
        Person p = (Person) em.find(Person.class, id);
        
        try{
        
         return p.getId();
        }
        catch(NullPointerException e){
            return new Long(-1);
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ent;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author vsa
 */
@Entity
@Table(name = "T_OSOBA") 
public class Osoba implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String meno;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date narodena;
    @Column(nullable=false)
    private Float vaha;

    public String getMeno() {
        return meno;
    }

    public Osoba(String meno, Date narodena, Float vaha) {
        this.meno = meno;
        this.narodena = narodena;
        this.vaha = vaha;
    }

    public Osoba() {
    }

    public Date getNarodena() {
        return narodena;
    }

    public void setMeno(String meno) {
        this.meno = meno;
    }

    public void setNarodena(Date narodena) {
        this.narodena = narodena;
    }

    public void setVaha(Float vaha) {
        this.vaha = vaha;
    }

    public Float getVaha() {
        return vaha;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Osoba)) {
            return false;
        }
        Osoba other = (Osoba) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Osoba[ id=" + id + " ]";
    }
    
}

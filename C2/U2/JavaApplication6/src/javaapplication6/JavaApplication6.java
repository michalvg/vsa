/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication6;

import ent.Osoba;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author vsa
 */
public class JavaApplication6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("JavaApplication6PU");
        EntityManager em = emf.createEntityManager();
        
        
        
        Osoba osoba = new Osoba();
        osoba.setMeno("Jozo");
        osoba.setVaha(new Float(125));
        osoba.setNarodena(new Date(95, 8, 19));
        osoba.setId(new Long(16));
        
        Osoba osoba2 = new Osoba("Brano", new Date(95, 3, 5),  new Float(88));
        osoba2.setId(new Long(14));
        
        Osoba osoba3 = new Osoba();
        osoba3.setId(new Long(15));
        //try{
        em.getTransaction().begin();
        em.persist(osoba);
        em.persist(osoba2);
        em.persist(osoba3);
        em.getTransaction().commit();
        //}
        //catch(Exception e){System.out.println(e.toString());}
        
        System.out.println(osoba.toString());
        System.out.println(osoba2.toString());
        System.out.println(osoba3.toString());
        
    }
    
}

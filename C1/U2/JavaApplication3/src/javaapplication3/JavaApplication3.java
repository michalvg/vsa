/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication3;
import java.sql.*;

/**
 *
 * @author vsa
 */
public class JavaApplication3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        // TODO code application logic here
        //a
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/sample", "app", "app");
        Statement st = con.createStatement();
        //b
        ResultSet rs = st.executeQuery("SELECT * FROM kniha");
        while (rs.next()) {
             System.out.println(rs.getString(1));
        }
        //c
        ResultSet ra = st.executeQuery("SELECT count(*) FROM kniha");
        while (ra.next()) {
            System.out.println("" + ra.getInt(1));
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt3;

import ent.TOsoba;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author vsa
 */
public class Projekt3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Projekt3 show = new Projekt3();
        show.findAll();
        show.findByMeno();
    }
    
    public void findAll(){
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projekt3PU");
        EntityManager em = emf.createEntityManager();
        
        TypedQuery<TOsoba> all = em.createNamedQuery("TOsoba.findAll", TOsoba.class);
        
        for(TOsoba o: all.getResultList()){
            System.out.println("osoba id: "+o.getId());
        }
        
    }
    
    public void findByMeno(){
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projekt3PU");
        EntityManager em = emf.createEntityManager();
        
        TypedQuery<TOsoba> q = em.createNamedQuery("TOsoba.findByMeno", TOsoba.class);
        
        q.setParameter("meno", "Jozo");
        for (TOsoba o: q.getResultList()){
            System.out.println(o.getId());
        }
        
    }
    
}

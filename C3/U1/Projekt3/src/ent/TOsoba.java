/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ent;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author vsa
 */
@Entity
@Table(name = "T_OSOBA")
@NamedQueries({
    @NamedQuery(name = "TOsoba.findAll", query = "SELECT t FROM TOsoba t"),
    @NamedQuery(name = "TOsoba.findById", query = "SELECT t FROM TOsoba t WHERE t.id = :id"),
    @NamedQuery(name = "TOsoba.findByMeno", query = "SELECT t FROM TOsoba t WHERE t.meno = :meno"),
    @NamedQuery(name = "TOsoba.findByNarodena", query = "SELECT t FROM TOsoba t WHERE t.narodena = :narodena"),
    @NamedQuery(name = "TOsoba.findByVaha", query = "SELECT t FROM TOsoba t WHERE t.vaha = :vaha")})
public class TOsoba implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "MENO")
    private String meno;
    @Column(name = "NARODENA")
    @Temporal(TemporalType.DATE)
    private Date narodena;
    @Basic(optional = false)
    @Column(nullable = false, name = "VAHA")
    private double vaha;

    public TOsoba() {
    }

    public TOsoba(Long id) {
        this.id = id;
    }

    public TOsoba(Long id, double vaha) {
        this.id = id;
        this.vaha = vaha;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMeno() {
        return meno;
    }

    public void setMeno(String meno) {
        this.meno = meno;
    }

    public Date getNarodena() {
        return narodena;
    }

    public void setNarodena(Date narodena) {
        this.narodena = narodena;
    }

    public double getVaha() {
        return vaha;
    }

    public void setVaha(double vaha) {
        this.vaha = vaha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TOsoba)) {
            return false;
        }
        TOsoba other = (TOsoba) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ent.TOsoba[ id=" + id + " ]";
    }
    
    
    
}
